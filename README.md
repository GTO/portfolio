# Portfolio

Cette application propose une landing page pour diffuser son CV ainsi que d'un module de blog pour diffuser ses actualités.

## Installation

Télécharger le contenu du dépôt dans le repertoire public_html-/var/www/-htdocs

	git clone https://git.framasoft.org/GTO/portfolio.git

Créez un utilisateur et une base de données MySQL pour le blog 

Initialisez la base avec les commandes sql suivantes (contenues dans _data/init.db.sql)

	CREATE TABLE Post (
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		title VARCHAR(255) NOT NULL,
		post TEXT NOT NULL,
		author_id INT(11) NOT NULL,
		date_posted DATE NOT NULL,
		date_modified DATE
	)

	INSERT INTO Post VALUES (
		1,
		'First Post',
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non vehicula ipsum, non sollicitudin nisl. Nam sit amet condimentum libero. Donec non erat non odio eleifend auctor eu eu mi. Aliquam at maximus quam, quis gravida nunc. Aenean finibus eget lacus nec aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc efficitur, arcu sed porttitor fringilla, diam arcu sollicitudin mi, eu pulvinar lorem tortor eget sem. Proin laoreet sed lorem non ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed magna diam, dapibus quis lectus ullamcorper, imperdiet sodales libero. Proin vitae tristique elit. Vivamus et ipsum in ligula malesuada aliquet eget eu nisl.',
		1,
		NOW(),
		NOW()
	)

	CREATE TABLE User (
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		username VARCHAR(255) NOT NULL,
		password VARCHAR(255) NOT NULL,
		email    VARCHAR(255) NOT NULL,
		date_created DATE NOT NULL,
		last_connected DATE NOT NULL
	)

	INSERT INTO User VALUES (
		1,
		'admin',
		SHA2('adminpass', 256),
		'admin@localhost.info',
		NOW(),
		NOW()
	)

Dans _data/dbconfig.php, remplacez les valeurs par défaut par vos identifiants

	Config::set('db.host', '127.0.0.1');
	Config::set('db.user', 'blog');
	Config::set('db.pass', 'blog');
	Config::set('db.name', 'blog');

Vérifiez que le serveur web peut écrire dans le répertoire logs/

	# Au plus simple :
	chmod -R 777 logs/
	
Démarrez l'application

