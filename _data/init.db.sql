CREATE TABLE Post (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255) NOT NULL,
	post TEXT NOT NULL,
	author_id INT(11) NOT NULL,
	date_posted DATE NOT NULL,
	date_modified DATE
)

INSERT INTO Post VALUES (
	1,
	'First Post',
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non vehicula ipsum, non sollicitudin nisl. Nam sit amet condimentum libero. Donec non erat non odio eleifend auctor eu eu mi. Aliquam at maximus quam, quis gravida nunc. Aenean finibus eget lacus nec aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc efficitur, arcu sed porttitor fringilla, diam arcu sollicitudin mi, eu pulvinar lorem tortor eget sem. Proin laoreet sed lorem non ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed magna diam, dapibus quis lectus ullamcorper, imperdiet sodales libero. Proin vitae tristique elit. Vivamus et ipsum in ligula malesuada aliquet eget eu nisl.',
	1,
	NOW(),
	NOW()
)

CREATE TABLE User (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	email    VARCHAR(255) NOT NULL,
	date_created DATE NOT NULL,
	last_connected DATE NOT NULL
)

INSERT INTO User VALUES (
	1,
	'admin',
	SHA2('1RootRulz', 256),
	'contact@thecrowstudios.eu',
	NOW(),
	NOW()
)