<?php

class BlogController extends Controller
{	
	public function index() {
		$this->listAll();
	}

	public function listAll() {
		$posts = Collection::get('Post')->find(array(
			'orderBy' => 'date_posted DESC'));

		$view = new View('listAll', $this);
		$view->render(array(
			'posts' => $posts,
		));
	}

	public function show($id) {
		$post = Collection::get('Post')->findById($id);
		if ($post == null) {
			throw new Exception("Impossible de trouver le post $id", 1);
		}
		$user = Collection::get('User')->findById($id);
		if ($user == null) {
			throw new Exception("Impossible de trouver le post $id", 1);
		}

		$view = new View('show', $this);
		$view->render(array(
			'post' => $post,
			'user' => $user,
		));
	}

	public function add() {
		if ($this->request->isPost()) {
			$post = new Post();

			$post->title   = $this->request->post['title'];
			//$post->tags    = $this->request->post['tags'];
			$post->author_id = 1;
			$post->date_posted = date("Y-m-d");
			$post->date_modified = $post->date_posted;

			$content = $this->request->post['post'];
			$content = str_replace("<script", "", $content);
			$post->post = $content;


			$post->id = $post->save();
			Session::setFlash("Ajout de l'article ".$post->id." réussi.", "success");
			$this->redirect(PathHelper::url(array('c'=>'blog', 'a'=>'show', 'id'=>$post->id)));
		}

		$view = new View('add', $this);
		$view->render();
	}

	public function edit($id) {
		$post = Collection::get('Post')->findById($id);
		if ($post == null) {
			throw new Exception("Impossible de trouver le post $id", 1);
		}

		if($this->request->isPost()) {
			if (isset($this->request->post['title'])) {
				$post->title = $this->request->post['title'];
			}
			if (isset($this->request->post['post'])) {
				$post->post = $this->request->post['post'];
			}
			$post->date_modified = date("Y-m-d");

			$post->save();
			Session::setFlash("Modification de l'article ".$post->id." réussie.", "success");
			$this->redirect(PathHelper::url(array('c'=>'blog', 'a'=>'show', 'id'=>$post->id)));
		}

		$view = new View('edit', $this);
		$view->render(array(
			'post' => $post,
		));
	}

	public function delete($id) {
		$post = Collection::get('Post')->findById($id);
		if ($post == null) {
			throw new Exception("Impossible de trouver le post $id", 1);
		}

		$post->delete();	
		Session::setFlash("Article supprimé", "success");
		$this->redirect(PathHelper::url(array('c'=>'blog', 'a'=>'listAll')));
	}
}