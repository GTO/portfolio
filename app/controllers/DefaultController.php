<?php

use \Michelf\Markdown;

class DefaultController extends Controller {
	
	public function index() {

		$posts = Collection::get('Post')->find(array(
			'orderBy' => 'date_posted DESC LIMIT 0,5'));

		$view = new View('index', $this);
		$view->render(array(
			'posts' => $posts,
		));
	}

	public function mail() {
		if($this->request->isPost()) {
			if (!isset($this->request->post['content'])) {
				Session::setFlash('Une erreur s\'est produite lors de l\'envoi du mail', 'danger');
				return 	$this->index();
			}
			if (!isset($this->request->post['email'])) {
				Session::setFlash('Une erreur s\'est produite lors de l\'envoi du mail', 'danger');
				return 	$this->index();
			}
			if (!isset($this->request->post['name'])) {
				Session::setFlash('Une erreur s\'est produite lors de l\'envoi du mail', 'danger');
				return 	$this->index();
			}

			$to      = 'manucorbeau@gmail.com';
			$subject = 'Portfolio : mail reçu';
			$message = Markdown::defaultTransform($this->request->post['content']);
			$headers =  'From: '.$this->request->post['email']  . "\r\n" .
						'Reply-To: '.$this->request->post['email'] . "\r\n" .
						'X-Mailer: PHP/' . phpversion();

			if (mail($to, $subject, $message, $headers)) {
				Session::setFlash('Votre mail a bien été envoyé. Merci :)', 'success');
			} else {
				Session::setFlash('Une erreur s\'est produite lors de l\'envoi du mail', 'danger');
			}
		}

		$this->index();
	}

}