<?php

class ErrorController extends Controller
{
	public function e404() {
		$view = new View('e404', $this);
		$view->render();
	}
}