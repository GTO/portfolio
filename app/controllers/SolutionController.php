<?php

class SolutionController extends BackendController
{	
	public function show($id) {
		$solution = Collection::get('Solution')->findById($id);
		if ($solution == null) {
			throw new Exception("Impossible de trouver la solution $id", 1);
		}

		$view = new View('show', $this);
		$view->render(array(
			'solution' => $solution,
		));
	}

}