<?php

class UserController extends Controller 
{
	public function __beforeAction() {
		// If non authenticated : go to login page
		if(!Session::isAuthenticated() && Session::get('user.id') == null) {
			$this->login();
		}
	}

	public function index() {
		$this->home;
	}
	public function home() {
		$view = new View('home', $this);
		$view->render();
	}


	public function login() {
		if (Session::isAuthenticated()) {
			$redirect = array(
				'c' => 'blog',
			);
			Session::setFlash('Vous êtes déjà connecté.', 'error');
			$this->redirect(PathHelper::url($redirect));
		}

		if ($this->request->isPost()) {
			$email = $this->request->post['email'];
			$password = $this->request->post['password'];


			$this->sanitize($email, 'email');
			$this->sanitize($password, 'password');

			$user = Collection::get('User')->findOne(array(
				'where' => array(
					'email'=> $email,
					'password'=> User::encrypt($password),
				)
			));

			if ($user != null) {
				Session::setAuthenticated(true);
				Session::set('user.id', $user->id);
				Session::set('user.username', $user->username);
				Session::set('user.email', $user->email);

				$user->last_connected = date("Y-m-d");
				$user->save();

				Session::setFlash('Vous êtes maintenant connecté', 'success');
				$this->redirect(PathHelper::url(array("c"=>"user","a"=>"home")));
			}
		}

		$view = new View('login', $this);
		$view->render();
	}

	public function logout() {
		if (!Session::isAuthenticated()) {
			$redirect = array(
				'c' => 'blog',
			);
			Session::setFlash('Vous devez être connecté pour voir cette page.', 'error');
			$this->redirect(PathHelper::url($redirect));
		}
		else {
			Session::setAuthenticated(false);
			Session::remove('user.id');
			Session::remove('user.username');
			Session::remove('user.email');
			Session::setFlash('Deconnexion réussie.', 'success');
			$this->redirect(PathHelper::url());
		}
	}

	private function sanitize(&$input, $type) {
		if ($type == 'email') {
			$input = trim($input);
			$input = stripslashes($input);
			$input = htmlspecialchars($input);
		}
		else {
			$input = trim($input);
			$input = stripslashes($input);
			$input = htmlspecialchars($input);
		}
	}
}