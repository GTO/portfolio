<?php

class Post extends Model
{
    public $id;
    public $title;
    public $post;
    public $author_id;
    public $date_posted;
    public $date_modified;

    public $tags;
}