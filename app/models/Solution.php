<?php

class Solution extends Model
{
	public $database="studio";
	
    public $id;
    public $title;
    public $resume;
    public $img_path;
    public $content;
    public $author_id;
    public $date_posted;
    public $date_modified;
    public $page_hits;
}