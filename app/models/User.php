<?php

class User extends Model
{
    public $id;
    public $username;
    public $password;
    public $email;
    public $date_created;
    public $last_connected;

    public static function encrypt($password) {
    	return hash('sha256', $password);
    }
}