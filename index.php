<?php

	date_default_timezone_set('Europe/Paris');

	function debug($data) {
		echo "<pre>" . print_r($data, true) . "</pre>";
	}

	function __autoload($name) {
		if (file_exists('lib/'.$name.'.php')) {
			require_once('lib/'.$name.'.php');
			return;
		}
		if (file_exists('app/controllers/'.$name.'.php')) {
			require_once('app/controllers/'.$name.'.php');
			return;
		}
		if (file_exists('app/models/'.$name.'.php')) {
			require_once('app/models/'.$name.'.php');
			return;
		}
		if (file_exists('vendors/'.$name.'.php')) {
			require_once('vendors/'.$name.'.php');
			return;
		}
	}

	//require_once('lib/Config.php');
	//require_once('lib/HttpRequest.php');
	//require_once('lib/Router.php');

	require_once('_data/dbconfig.php');
	require_once('vendors/Michelf/Markdown.inc.php');

	Config::set('app.url', 'thecrowstudio.eu');
	Config::set('app.title', 'Portfolio');
	Config::set('app.root', dirname(__FILE__).'/');
	Config::set('app.version', '1');

	Config::set('solutions' , 'Solutions');
	Config::set('blog' , 'Blog');
	Config::set('about', 'About');
		Config::set('about.photo', 'web/assets/profile.png');
		Config::set('about.photo.alt', 'Generic placeholder image');
	Config::set('cv', 'CV');
	Config::set('social'   , 'Social');
	Config::set('projects' , 'Projects');
	Config::set('location' , 'Location');
	Config::set('contact'  , 'Contact');
	Config::set('copyright', '&copy; The Crow Studios - 2016');


	$request = new HttpRequest();
	$router = new Router($request);

	try {
		$router->run();
	} catch(Exception $e) {
		App::log($e->getMessage());
		$error = new ErrorController($request);
		$error->e404();
	}

