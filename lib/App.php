<?php

class App {
	public static function debug($something) {
		return "<pre>\n".var_dump($something)."\n".print_r($something)."</pre>";
	}

	public static function log($something, $file="error") {
		$log = "[".date("Y-m-d H:i:s")."] $something\n";
		$file = dirname(__FILE__).'/../logs/'.$file.'.log';
		error_log($log, 3, $file);
	}
}