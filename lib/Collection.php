<?php

class Collection
{
	public $model;

	public static function get($modelName) {
		return new Collection($modelName);
	}

	public function __construct($model) {
		$this->model = $model;
	}

	public function find($options = array()) {
		$default = array(
			'where' => array(),
			'groupBy' => '',
			'offset' => 0,
			'limit' => -1,
			'orderBy' => '',
		);
		$options = array_merge($default, $options);

		try {
			$db = Connection::getInstance();
		} 
		catch (Exception $e) {
			throw $e;
		}

		$query  = 'SELECT * FROM '.$this->model.' ';

		if (!empty($options['where'])) {
			$query .= 'WHERE ';
			foreach ($options['where'] as $key => $value) {
				$query .= " $key=:$key AND";
			}
			$query = substr($query, 0, strlen($query) - 3);
		}

		if (!empty($options['groupBy'])) {
			$query .= ' GROUP BY '.$options['groupBy'].' ';
		}
		
		if (!empty($options['orderBy'])) {
			$query .= ' ORDER BY '.$options['orderBy'].' ';
		}

		if ($options['limit'] > $options['offset']) {
			$query .= ' LIMIT '.$options['limit'].' OFFSET '.$options['offset'].' ';
		}



		$stmt = $db->prepare($query);

		$bindParams = array();
		if (!empty($options['where'])) {
			foreach ($options['where'] as $key => $value) {
				$newKey = ':'.$key;
				$bindParams[$newKey] = $value;
			}
		}

		App::log(Connection::interpolateQuery($query, $bindParams), 'sql');

		try {
			$db->beginTransaction();
			$stmt->execute($bindParams);
			$db->commit();
			return $stmt->fetchAll(PDO::FETCH_CLASS, $this->model);
			//$res = $stmt->fetchAll(PDO::FETCH_CLASS, $this->model);
			//debug($res);
			//return $res;
		} catch (Exception $e) {
			$db->rollBack();
			//echo "Failed: " . $e->getMessage();
		}
	}

	public function findAll() {
		return $this->find();
	}

	public function findById($id) {
		$elem = $this->find(array(
			'where' => array(
				'id'=> $id,
			),
		));
		return $elem[0];
	}

	public function findOne($options) {
		$r = $this->find($options);
		return $r[0];
	}
}