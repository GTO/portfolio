<?php
class Connection extends PDO
{
    private static $instance = NULL;
    private static $canInstanciate = false;

    protected $link;
    private $dsn, $username, $password, $options;


    public static function getInstance($options = array())
    {
        if (!isset(self::$instance))
        {
            self::$canInstanciate = true;
            try {
                self::$instance = new Connection($options);
            } catch(Exception $e) {
                throw $e;
            }
            self::$canInstanciate = false;
        }
        return self::$instance;
    }

    
    public function __construct($options = array())
    {      
        $default = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );
        $options = array_merge($default, $options);

        if (self::$canInstanciate != true) {
            trigger_error (__CLASS__.' is not supposed to be instantiated from the global scope because it is a Singleton class.', E_USER_ERROR);
        } 
        else {
            try {
                $this->dsn = 'mysql:host='.Config::get('db.host').';dbname='.Config::get('db.name');
                $this->username = Config::get('db.user');
                $this->password = Config::get('db.pass');
                $this->options = $options;
                $this->connect();
            } 
            catch (PDOException $e) {
                throw $e;
            }
        }
    }

    private function connect()
    {
        $this->link = parent::__construct($this->dsn, $this->username, $this->password, $this->options);
    }

    public function __clone()
    {
        trigger_error('Cloning a singleton is not allowed.', E_USER_ERROR);
    }

    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from 
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     * @return string The interpolated query
     */
    public static function interpolateQuery($query, $params) {
        $keys = array();

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        #trigger_error('replaced '.$count.' keys');

        return $query;
    }
}