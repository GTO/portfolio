<?php

class Controller
{
	public $name;
	public $request;

	public function __construct($request) {
		$name = explode("Controller", get_class($this));
		$this->name = $name[0];
		
		$this->request = $request;
	}

	public function __beforeAction() {
		return;
	}

	public function __afterAction() {
		return;
	}

	public function redirect($url) {
		header("Location: $url");
		exit();
	}
}