<?php


class HttpRequest
{
	public $method;
	public $time;
	public $queryString;
	public $httpAccept;
	public $httpAcceptCharset;
	public $httpAcceptEncoding;
	public $httpAcceptLanguage;
	public $httpConnection;
	public $httpHost;
	public $httpReferer;
	public $httpUserAgent;
	public $https;
	public $remoteAddr;
	public $remoteHost;
	public $remotePort;
	public $remoteUser;
	public $redirectRemoteUser;
	public $requestURI;

	public $params;
	public $post;
	public $get;

	public function __construct() {
		$this->method      = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
		$this->time        = isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : null;
        $this->queryString = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null;
		$this->httpAccept  = isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : null;
		$this->httpAcceptCharset  = isset($_SERVER['HTTP_ACCEPT_CHARSET']) ? $_SERVER['HTTP_ACCEPT_CHARSET'] : null;
		$this->httpAcceptEncoding = isset($_SERVER['HTTP_ACCEPT_ENCODING']) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : null;
		$this->httpAcceptLanguage = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : null;
		$this->httpConnection = isset($_SERVER['HTTP_CONNECTION']) ? $_SERVER['HTTP_CONNECTION'] : null;
		$this->httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null;
		$this->httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
		$this->httpUserAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
		$this->https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : null;
		$this->remoteAddr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
		$this->remoteHost = isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : null;
		$this->remotePort = isset($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : null;
		$this->remoteUser = isset($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] : null;
		$this->redirectRemoteUser = isset($_SERVER['REDIRECT_REMOTE_USER']) ? $_SERVER['REDIRECT_REMOTE_USER'] : null;
		$this->requestURI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;

		parse_str($this->queryString, $this->params);

		if ($this->isPost()) {
			$this->post = $_POST;
		}
		if ($this->isGet()) {
			$this->get = $_GET;
		}
	}

	public function isPost() {
		if(strtolower($this->method) == 'post') {
			return true;
		}
		return false;
	}

	public function isGet() {
		if(strtolower($this->method) == 'get') {
			return true;
		}
		return false;
	}

	public function isXmlHttpRequest() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return true;
		}
		return false;
	}
}