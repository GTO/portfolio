<?php

class Model {
	public $name;

	public function __construct() {
		$this->name = get_class($this);
	}

	public function save() {
		if ($this->id) {
			return $this->update();
		}
		else {
			return $this->insert();
		}
	}

	public function delete() {
		try {
			$db = Connection::getInstance();
		} 
		catch (Exception $e) {
			throw $e;
		}

		$query = "DELETE FROM ".$this->name." ";
		$query.= "WHERE id=:id;";

		$stmt = $db->prepare($query);	

		App::log(Connection::interpolateQuery($query, array(':id'=>$this->id)), 'sql');
		
		try {
			$db->beginTransaction();
			$stmt->execute(array(':id'=>$this->id));
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	protected function update() {
		try {
			$db = Connection::getInstance();
		} 
		catch (Exception $e) {
			throw $e;
		}
		$vars = get_object_vars($this);
		unset($vars['name']);

		$query = 'UPDATE '.$this->name.' ';
		$query.= 'SET ';		
		foreach ($vars as $key => $value) {
			if ($value != NULL && $key != 'id') {
				$query .= " $key=:$key,";
			}
		}
		$query = substr($query, 0, strlen($query)-1);
		$query.= " WHERE ";
		$query.= "id=:id;";	

		$stmt = $db->prepare($query);	

		foreach ($vars as $key => $value) {
			if ($value != NULL) {
				$stmt->bindValue(":$key", $value);	
			}
		}		
		App::log(Connection::interpolateQuery($query, $vars), 'sql');

		try {
			$db->beginTransaction();
			$stmt->execute();
			$id = $db->lastInsertId();
			$db->commit();
			return $id;
		} catch (Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}
	protected function insert() {
		try {
			$db = Connection::getInstance();
		} 
		catch (Exception $e) {
			throw $e;
		}

		$query = 'INSERT INTO '.$this->name.' (';
		$vars = get_object_vars($this);
		unset($vars['name']);
		foreach ($vars as $key => $value) {
			if ($value != NULL) {
				$query .= $key.',';
			}
		}
		$query = substr($query, 0, strlen($query)-1);
		$query .= ') VALUES (';
		foreach ($vars as $key => $value) {
			if ($value != NULL) {
				$query .= ":$key,";
			}
		}
		$query = substr($query, 0, strlen($query)-1);
		$query.= ');';
		$stmt = $db->prepare($query);	

		foreach ($vars as $key => $value) {
			if ($value != NULL) {
				$stmt->bindValue(":$key", $value);	
			}
		}
		App::log(Connection::interpolateQuery($query, $vars), 'sql');


		try {
			$db->beginTransaction();
			$stmt->execute();
			$id = $db->lastInsertId();
			$db->commit();
			return $id;
		} catch (Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}
}