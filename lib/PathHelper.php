<?php

class PathHelper
{
	public static function url($params = array()) {
		$baseURL = explode('?', $_SERVER['REQUEST_URI']);
		$baseURL = $baseURL[0];

		$url = "?";
		foreach ($params as $key => $value) {
			$url .= $key . "=" . $value . "&";
		}
		$url = substr($url, 0, strlen($url)-1);

		return $baseURL.$url;
	}
}