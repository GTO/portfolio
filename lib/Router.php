<?php


class Router 
{
	protected $request = null;

	protected $controller = null;
	protected $action = null;
	protected $params = array();

	public function __construct($request, $defaults = array())
	{
		
		$options = array(
			'controller' => 'default',
			'action' => 'index',
		);
		$options = array_merge($options, $defaults);

		$this->request = $request;
		if (isset($this->request->params['c'])) {
			$this->controller = $this->request->params['c'];
			if (isset($this->request->params['a'])) {
				$this->action = $this->request->params['a'];
			}
			else {
				$this->action = $options['action'];
			}
		}
		else {
			$this->controller = $options['controller'];
			$this->action = $options['action'];
		}

		$this->params = $this->request->params;
		unset($this->params['c']);
		unset($this->params['a']);
	}

	public function run() {
		$controller = null;
		$params = $this->params;
		$className = ucfirst($this->controller).'Controller';

		if (class_exists($className)) {
			$controller = new $className($this->request);

			if (method_exists($controller, $this->action)) {
				call_user_method('__beforeAction', $controller);
				call_user_func_array(array($controller,$this->action), $this->params);
				call_user_method('__afterAction' , $controller);
			}
			else {
				throw new Exception("Huh... 404 ? -- no action (in Router::run())", 1);
			}
		} else {
			throw new Exception("Huh... 404 ? -- no controller (in Router::run())", 1);
		}
	}
}