<?php

if (false == session_id()) {
    session_start();
}

class Session {
    public static function getFlash() {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }    
    public static function getFlashLevel() {
        $level = $_SESSION['flash.level'];
        return $level;
    }    
    public static function hasFlash() {
        return isset($_SESSION['flash']);
    }    
    public static function setFlash($value, $level='error') {
        $_SESSION['flash'] = $value;
        $_SESSION['flash.level'] = $level;
    }

    public static function isAuthenticated() {
        if($_SESSION['auth'] === true) {
            return true;
        }
        return false;
    }   

    public static function setAuthenticated($bool = true) {
        $_SESSION['auth'] = $bool;
    }

    public static function get($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    } 

    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    } 

    public static function remove($key) {
        if(isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }
}