<?php

class ToolsHelper {

    public static function dateFormat($str_date, $new_format) {
    	$date = new DateTime($str_date);
		return $date->format($new_format);
    }

}