<?php

class View {
	public $name;
	public $controller;
	protected $params = array();

	public function __construct($name, &$controller) {
		$this->name = $name;
		$this->controller = $controller;
	}

	public function __get($name) {
		return $this->params[$name];
	}

    public function addParam($p, $v)
    {
        if (!is_string($p) || is_numeric($p) || empty($p))
        {
            throw new InvalidArgumentException('Le nom de la variable doit être une chaine de caractère non nulle');
        }
        $this->params[$p] = $v;
    }


	public function renderPartial($partialName, $getAsString = false) {
		$path = __DIR__.'/../app/views/_partials/'.$partialName.'.html';
		if (file_exists($path)) {
			ob_start();
				require($path);
			$partial_content = ob_get_clean();

			if ($getAsString) {
				return $partial_content;
			}
			echo $partial_content;
		}

		return null;
	} 

	public function render($params = array()) {
		$path = __DIR__.'/../app/views/'.$this->controller->name.'/'.$this->name.'.html';
		if (!file_exists($path)) {
			throw new Exception("Huh... -- no view (in View::render())", 1);
		}

		$this->params = $params;
		extract($params);

		ob_start();
			require($path);
		$page_content = ob_get_clean();

		$layout = __DIR__.'/../app/views/layout.html';
		if (file_exists($layout)) {
			ob_start();
				require($layout);
			die(ob_get_clean());
		} 
		else {
			die($page_content);
		}
	}
}