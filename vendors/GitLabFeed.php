<?php
class GitLabCommit
{
	var $id;
	var $link;
	var $title;
	var $updated;
	var $summary;
}

class GitLabFeed
{
    var $commits = array();

    function __construct($url)
    {
        if (!($x = simplexml_load_file($url)))
            return;

        foreach ($x->entry as $entry)
        {
            $commit = new GitLabCommit();
            $commit->id  = (string) $entry->id;
            $commit->updated = strtotime($entry->updated);
            $commit->link  = (string) $entry->link["href"];
            $commit->title = (string) $entry->title;
            $commit->summary  = (string) $entry->summary->asXML();

            $commit->title = str_replace("GTO pushed to project b", "B", $commit->title);
            $commit->summary = str_replace("<blockquote>", "", $commit->summary);
            $commit->summary = str_replace("</blockquote>", "", $commit->summary);

            $this->commits[] = $commit;
        }
    }
}